#!/bin/sh
# LIBS
source brownsh/ostype.sh
loadOSLibs 

# SETUP
## HUMAN
mainCfg="main.cfg"

## STATIC
staticRoot="C:\Repositories\bagi"
timest=$(timestamplog)
echo "timest: ${timest}"
## OUTPUTS 
log="${staticRoot}/logs/${timest}_main.log"
out="${staticRoot}/out/${timest}_main.txt"

# MAIN
startlog "Starting Log"

echolog "#A00 Activate environment"
bagienv/Scripts/activate.bat >> $log

echolog "#A01 Change position"
cd bnn >> $log

echolog "#A02 Execute"
python bnn.py >> $out

echolog "#Z01 Finish"
echo "Press anything to finish..."
read -s $anything