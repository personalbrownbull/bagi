# BAGI

## Clone
```sh
git clone git@bitbucket.org:personalbrownbull/bagi.git
```

## Submodules
### Add
```sh
git submodule add <url> subName
```

### Get Code
```sh
git submodule update --init --recursive
```

### Update Code
```sh
git submodule update --remote --merge
```

### Push Changes
based on [this]
```sh
# cd submoule
cd bnn
git add --all
git commit -m "message"
git push origin HEAD:master
cd ..
# git add  submodule
git add bnn
git commit -m "updated submodule"
git push
```

# Setup
## Graphviz in Windows
Download from [here](https://graphviz.org/download/), then install it

## Virtual Environment
```sh
cd bagi
python -m venv bagienv
bagienv\Scripts\activate.bat
pip install -r bnn/requirements.txt
```

## Test
```sh
python BNN.py
```